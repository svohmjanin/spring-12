package ru.speedway.spring12.domain;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Getter
@Setter
@ToString
@NoArgsConstructor
@RequiredArgsConstructor
@Document(collection = "book")
public class Book {
    @Id
    private String id;

    @NonNull
    @Field
    private String name;

    @NonNull
    @Field
    private Author author;

    @NonNull
    @Field
    private Genre genre;

}
