package ru.speedway.spring12.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.speedway.spring12.domain.Genre;
import ru.speedway.spring12.service.GenrePrintService;
import ru.speedway.spring12.service.GenreService;

import java.util.List;

@ShellComponent
public class GenreShell {

    private GenreService genreService;
    private GenrePrintService genrePrintService;

    public GenreShell(GenreService genreService, GenrePrintService genrePrintService) {
        this.genreService = genreService;
        this.genrePrintService = genrePrintService;
    }

    @ShellMethod("Genres list.")
    public void genreList() {

        List<Genre> genres = genreService.getAll();

        genrePrintService.list(genres);
    }

    @ShellMethod("Add genre.")
    public String addGenre(
            @ShellOption({"-N", "--name"}) String name
    ) {

        try {
            genreService.add(name);
        } catch (Exception e) {
            return e.getMessage();
        }

        return "Row added";
    }

    @ShellMethod("Delete genre.")
    public String deleteGenre(
            @ShellOption({"-I", "--id"}) String id
    ) {

        try {
            genreService.delete(id);
        } catch (Exception e) {
            return e.getMessage();
        }

        return "Row deleted";
    }
}
