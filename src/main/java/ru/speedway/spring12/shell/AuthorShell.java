package ru.speedway.spring12.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.speedway.spring12.domain.Author;
import ru.speedway.spring12.service.AuthorPrintService;
import ru.speedway.spring12.service.AuthorService;

import java.util.List;

@ShellComponent
public class AuthorShell {

    private AuthorService authorService;
    private AuthorPrintService authorPrintService;

    public AuthorShell(AuthorService authorService, AuthorPrintService authorPrintService) {
        this.authorService = authorService;
        this.authorPrintService = authorPrintService;
    }

    @ShellMethod("Authors list.")
    public void authorList() {

        List<Author> authors = authorService.getAll();

        authorPrintService.list(authors);
    }

    @ShellMethod("Add author.")
    public String addAuthor(
            @ShellOption({"-N", "--name"}) String name
    ) {

        try {
            authorService.add(name);
        } catch (Exception e) {
            return e.getMessage();
        }

        return "Row added";
    }

    @ShellMethod("Delete author.")
    public String deleteAuthor(
            @ShellOption({"-I", "--id"}) String id
    ) {

        try {
            authorService.delete(id);
        } catch (Exception e) {
            return e.getMessage();
        }

        return "Row deleted";
    }
}
