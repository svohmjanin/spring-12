package ru.speedway.spring12.shell;

import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import ru.speedway.spring12.domain.Book;
import ru.speedway.spring12.service.BookPrintService;
import ru.speedway.spring12.service.BookService;

import java.util.List;

@ShellComponent
public class BookShell {

    private BookService bookService;
    private BookPrintService bookPrintService;

    public BookShell(BookService bookService, BookPrintService bookPrintService) {
        this.bookService = bookService;
        this.bookPrintService = bookPrintService;
    }

    @ShellMethod("Books list.")
    public void bookList() {

        List<Book> books = bookService.getAll();

        bookPrintService.list(books);
    }

    @ShellMethod("Add book.")
    public String addBook(
            @ShellOption({"-N", "--name"}) String name,
            @ShellOption({"-A", "--author"}) String author,
            @ShellOption({"-G", "--genre"}) String genre
    ) {

        try {
            bookService.add(name, author, genre);
        } catch (Exception e) {
            return e.getMessage();
        }

        return "Row added";
    }

    @ShellMethod("Delete book.")
    public String deleteBook(
            @ShellOption({"-I", "--id"}) String id
    ) {

        try {
            bookService.delete(id);
        } catch (Exception e) {
            return e.getMessage();
        }

        return "Row deleted";
    }
}
