package ru.speedway.spring12.service;

import ru.speedway.spring12.domain.Book;

import java.util.List;

public interface BookPrintService {
    void list(List<Book> authors);
}
