package ru.speedway.spring12.service;

import ru.speedway.spring12.domain.Author;

import java.util.List;

public interface AuthorPrintService {

    void list(List<Author> authors);
}
