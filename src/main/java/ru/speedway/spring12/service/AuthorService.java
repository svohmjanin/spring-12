package ru.speedway.spring12.service;

import ru.speedway.spring12.domain.Author;

import java.util.List;

public interface AuthorService {

    Author add(String name);
    void delete(String id);
    Author getByName(String name);
    Author edit(Author author);
    List<Author> getAll();
}
