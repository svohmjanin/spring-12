package ru.speedway.spring12.service;

import ru.speedway.spring12.domain.Genre;

import java.util.List;

public interface GenreService {
    Genre add(String name);
    void delete(String id);
    Genre getByName(String name);
    Genre edit(Genre author);
    List<Genre> getAll();
}
