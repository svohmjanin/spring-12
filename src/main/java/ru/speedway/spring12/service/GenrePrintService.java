package ru.speedway.spring12.service;

import ru.speedway.spring12.domain.Genre;

import java.util.List;

public interface GenrePrintService {

    void list(List<Genre> genres);
}
