package ru.speedway.spring12.service;

import ru.speedway.spring12.domain.Book;

import java.util.List;

public interface BookService {
    Book add(String name, String authorName, String genreName);
    void delete(String id);
    Book getByName(String name);
    Book edit(String bookId, String name, String authorName, String genreName);
    List<Book> getAll();
}
