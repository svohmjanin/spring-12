package ru.speedway.spring12.service.impl;

import org.springframework.stereotype.Service;
import ru.speedway.spring12.domain.Author;
import ru.speedway.spring12.repository.AuthorRepository;
import ru.speedway.spring12.service.AuthorService;

import java.util.List;

@Service
public class AuthorServiceImpl implements AuthorService {

    private AuthorRepository authorRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public Author add(String name) {
        Author author = new Author(name);
        return authorRepository.insert(author);
    }

    @Override
    public void delete(String id) {
        Author author = authorRepository.getById(id);
        authorRepository.delete(author);
    }

    @Override
    public Author getByName(String name) {
        return null;
    }

    @Override
    public Author edit(Author author) {
        return  authorRepository.save(author);
    }

    @Override
    public List<Author> getAll() {
        return authorRepository.findAll();
    }
}
