package ru.speedway.spring12.service.impl;

import org.springframework.stereotype.Service;
import ru.speedway.spring12.domain.Author;
import ru.speedway.spring12.service.AuthorPrintService;

import java.util.List;

@Service
public class AuthorPrintServiceImpl implements AuthorPrintService {

    @Override
    public void list(List<Author> authors) {

        System.out.println("Authors list:");

        for (Author author : authors) {
            System.out.println(String.format(" %s | %s ", author.getId(), author.getName()));
        }

        System.out.println(String.format("Row count: %d", authors.size()));
    }
}
