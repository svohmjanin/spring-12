package ru.speedway.spring12.service.impl;

import org.springframework.stereotype.Service;
import ru.speedway.spring12.domain.Genre;
import ru.speedway.spring12.service.GenrePrintService;

import java.util.List;

@Service
public class GenrePrintServiceImpl implements GenrePrintService {

    @Override
    public void list(List<Genre> genres) {

        System.out.println("Genres list:");

        for (Genre genre : genres) {
            System.out.println(String.format(" %s | %s ", genre.getId(), genre.getName()));
        }

        System.out.println(String.format("Row count: %d", genres.size()));
    }
}
