package ru.speedway.spring12.service.impl;

import org.springframework.stereotype.Service;
import ru.speedway.spring12.domain.Genre;
import ru.speedway.spring12.repository.GenreRepository;
import ru.speedway.spring12.service.GenreService;

import java.util.List;

@Service
public class GenreServiceImpl implements GenreService {

    private GenreRepository genreRepository;

    public GenreServiceImpl(GenreRepository genreRepository) {
        this.genreRepository = genreRepository;
    }

    @Override
    public Genre add(String name) {
        Genre genre = new Genre(name);
        return genreRepository.insert(genre);
    }

    @Override
    public void delete(String id) {
        Genre genre = genreRepository.getById(id);
        genreRepository.delete(genre);
    }

    @Override
    public Genre getByName(String name) {
        return null;
    }

    @Override
    public Genre edit(Genre genre) {
        return  genreRepository.save(genre);
    }

    @Override
    public List<Genre> getAll() {
        return genreRepository.findAll();
    }
}
