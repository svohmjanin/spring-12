package ru.speedway.spring12.service.impl;

import org.springframework.stereotype.Service;
import ru.speedway.spring12.domain.Author;
import ru.speedway.spring12.domain.Book;
import ru.speedway.spring12.service.BookPrintService;

import java.util.List;

@Service
public class BookPrintServiceImpl implements BookPrintService {

    @Override
    public void list(List<Book> books) {

        System.out.println("Book list:");

        for (Book book : books) {
            System.out.println(String.format(" %s | %s | %s", book.getId(), book.getName(), book.getAuthor().getName()));
        }

        System.out.println(String.format("Row count: %d", books.size()));
    }
}
