package ru.speedway.spring12.service.impl;

import org.springframework.stereotype.Service;
import ru.speedway.spring12.domain.Author;
import ru.speedway.spring12.domain.Book;
import ru.speedway.spring12.domain.Genre;
import ru.speedway.spring12.repository.AuthorRepository;
import ru.speedway.spring12.repository.BookRepository;
import ru.speedway.spring12.repository.GenreRepository;
import ru.speedway.spring12.service.BookService;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    private BookRepository bookRepository;
    private AuthorRepository authorRepository;
    private GenreRepository genreRepository;

    public BookServiceImpl(BookRepository bookRepository, AuthorRepository authorRepository, GenreRepository genreRepository) {
        this.bookRepository = bookRepository;
        this.authorRepository = authorRepository;
        this.genreRepository = genreRepository;
    }

    @Override
    public Book add(String name, String authorName, String genreName) {
        Author author = authorRepository.getByName(authorName);
        if(author == null){
            author = new Author(authorName);
            authorRepository.insert(author);
        }

        Genre genre = genreRepository.getByName(genreName);
        if(genre == null){
            genre = new Genre(genreName);
            genreRepository.insert(genre);
        }

        Book book = new Book(name, author, genre);

        return bookRepository.insert(book);
    }

    @Override
    public void delete(String id) {
        Book book = bookRepository.getById(id);
        bookRepository.delete(book);
    }

    @Override
    public Book getByName(String name) {
        return null;
    }

    @Override
    public Book edit(String bookId, String name, String authorName, String genreName) {

        Book book = bookRepository.getById(bookId);

        Author author = authorRepository.getByName(authorName);
        if(author == null){
            author = new Author(authorName);
            authorRepository.insert(author);
        }

        Genre genre = genreRepository.getByName(genreName);
        if(genre == null){
            genre = new Genre(genreName);
            genreRepository.insert(genre);
        }

        book.setName(name);
        book.setAuthor(author);
        book.setGenre(genre);

        return  bookRepository.save(book);
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.findAll();
    }
}
