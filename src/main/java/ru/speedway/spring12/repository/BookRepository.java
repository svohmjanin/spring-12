package ru.speedway.spring12.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.speedway.spring12.domain.Book;

public interface BookRepository extends MongoRepository<Book, Long> {
    Book getByName(String name);
    Book getById(String name);
}
