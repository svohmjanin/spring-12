package ru.speedway.spring12.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.speedway.spring12.domain.Book;
import ru.speedway.spring12.domain.Genre;

public interface GenreRepository extends MongoRepository<Genre, Long> {
    Genre getByName(String name);
    Genre getById(String name);
}