package ru.speedway.spring12.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.speedway.spring12.domain.Author;

public interface AuthorRepository extends MongoRepository<Author, Long> {
    Author getByName(String name);
    Author getById(String name);
}
