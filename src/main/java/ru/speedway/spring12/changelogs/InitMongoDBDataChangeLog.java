package ru.speedway.spring12.changelogs;

import com.github.cloudyrock.mongock.ChangeLog;
import com.github.cloudyrock.mongock.ChangeSet;
import com.mongodb.client.MongoDatabase;
import lombok.val;
import org.springframework.data.mongodb.core.MongoTemplate;
import ru.speedway.spring12.domain.Author;
import ru.speedway.spring12.domain.Book;
import ru.speedway.spring12.domain.Genre;

@ChangeLog(order = "001")
public class InitMongoDBDataChangeLog {

    private Author author1;
    private Author author2;
    private Genre genre1;
    private Genre genre2;

    @ChangeSet(order = "000", id = "dropDB", author = "speedway", runAlways = true)
    public void dropDB(MongoDatabase database){
        database.drop();
    }

    @ChangeSet(order = "001", id = "initAuthors", author = "speedway", runAlways = true)
    public void initStudents(MongoTemplate template){
        author1 = template.save(new Author("Author 1"));
        author2 = template.save(new Author("Author 2"));
    }

    @ChangeSet(order = "002", id = "initGenres", author = "speedway", runAlways = true)
    public void initGenres(MongoTemplate template){
        genre1 = template.save(new Genre("Genre 1"));
        genre2 = template.save(new Genre("Genre 2"));
    }

    @ChangeSet(order = "003", id = "initBooks", author = "speedway", runAlways = true)
    public void initBooks(MongoTemplate template){
        val book1 = template.save(new Book("Book 1", author1, genre1 ));
        val book2 = template.save(new Book("Book 2", author2, genre2 ));
    }
}
