package ru.speedway.spring12.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.speedway.spring12.domain.Author;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("AuthorRepository должен ")
class AuthorRepositoryTest  extends AbstractRepositoryTest {

    @Autowired
    private AuthorRepository authorRepository;

    @DisplayName("возвращать автора по имени")
    @Test
    void getByName() {
        Author selectAuthor = authorRepository.save(new Author("Author for select"));

        assertThat(selectAuthor.getName()).isEqualTo("Author for select");
    }

    @DisplayName("добавлять автора")
    @Test
    void insert() {
        Author newAuthor = new Author("New author");
        authorRepository.save(newAuthor);

       assertThat(authorRepository.getByName("New author").getName()).isEqualTo("New author");
    }

    @DisplayName("удалять автора")
    @Test()
    void delete() {
        Author deleteAuthor = authorRepository.save(new Author("Author for delete"));

        Author testDeletedAuthor = authorRepository.getById(deleteAuthor.getId());
        authorRepository.delete(testDeletedAuthor);

        assertThat(authorRepository.getByName("Author for delete")).isEqualTo(null);
    }
}
