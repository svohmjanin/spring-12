package ru.speedway.spring12.repository;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.speedway.spring12.domain.Author;
import ru.speedway.spring12.domain.Book;
import ru.speedway.spring12.domain.Genre;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("BookRepository должен ")
public class BookRepositoryTest extends AbstractRepositoryTest {

    @Autowired
    private BookRepository bookRepository;

    @DisplayName("возвращать книгу по названию")
    @Test
    void getByName() {
        Book selectBook = bookRepository.save(new Book("Book for select", (new Author()), (new Genre())));

        assertThat(selectBook.getName()).isEqualTo("Book for select");
    }

    @DisplayName("добавлять книгу")
    @Test
    void insert() {
        Book newBook = bookRepository.save(new Book("New book", (new Author()), (new Genre())));

        assertThat(bookRepository.getById(newBook.getId()).getName()).isEqualTo("New book");
    }

    @DisplayName("удалять книгу")
    @Test()
    void delete() {
        Book deleteBook = bookRepository.save(new Book("Book for delete", (new Author()), (new Genre())));

        Book testDeletedBook = bookRepository.getById(deleteBook.getId());
        bookRepository.delete(testDeletedBook);

        assertThat(bookRepository.getByName("Book for delete")).isEqualTo(null);
    }

}
